package sheridan;

public class Primes {
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Primes.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Primes.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Primes.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Primes.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Primes.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + Primes.isPrime( 36 ) );				
		System.out.println( "Number 50 is prime? " + Primes.isPrime( 50 ) );	// change 1			
		System.out.println( "Number 51 is prime? " + Primes.isPrime( 51 ) );
		System.out.println( "Number 52 is prime? " + Primes.isPrime( 52 ) );// change 2					
		
	}
}
